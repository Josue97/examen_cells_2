import { LitElement, html, css } from 'lit-element';
import '@material/mwc-top-app-bar-fixed'
import '@material/mwc-button'

class BasicLitElement  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        persona:{type:String},
        personas:{type:String},
        regalo:{type:String},
        regalos:{type:Array}

    };
  }

  constructor() {
    super();
        this.persona = '';
        this.personas = [];
        this.regalo = '';
        this.regalos = [];
        
  }
  participante() {
    const persona = this.shadowRoot.querySelector('#nombre');
    this.personas.set(personas, []);
    persona.value = '';   
}
regalos() {
  
  const regalo = this.shadowRoot.querySelector('#regalo');
  const listaRegalos = []
  listaRegalos = [...this.listaRegalos, regalo.value];
  this.todos.set(key, listaRegalos);
  present.value = '';
}



  render() {
    return html`
    <mwc-top-app-bar-fixed centerTitle>
    <mwc-icon-button icon="menu" slot="navigationIcon"></mwc-icon-button>
    <div slot="title">Amigo secreto</div>
    <mwc-icon-button icon="favorite" slot="actionItems"></mwc-icon-button>
    <div><!-- content --></div>
    </mwc-top-app-bar-fixed>
      <div class= "container">
          <input id="nombre" type="text" placeholder="ej. Juan"
          @change="${(e)=> (this.todo = e.target.value)}" .value="${this.persona}"> 
          <mwc-button raised label="agregar" icon="code" @click="${this.participante}" ?disabled="${this.todo==''}"></mwc-button>
          ${this.todos.lenght ==0
          ? html` <p>Ningun participante seleccionado</p> `
          :html`
            
          ` }
      </div>
    `;
  }
}

customElements.define('basic-lit-element', BasicLitElement);